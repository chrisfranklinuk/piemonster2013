from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _
from autoslug import AutoSlugField


class Skill(models.Model):

    """
    Store and manage Skill objects
    """

    name = models.CharField(_('name'), help_text=_('Name of the object'), max_length=255)
    slug = AutoSlugField(populate_from='name')

    class Meta:
        verbose_name = _('Skill')
        verbose_name_plural = _('Skills')

    def __unicode__(self):
        return 'Skill ({})'.format(self.id or 'Unsaved')

    def get_absolute_url(self):
        return reverse('skill_detail', args=[str(self.id)])

    def get_api_url(self):
        return reverse('skill-detail', args=[str(self.id)])
