from rest_framework import viewsets

from .models import Skill
from .serializers import SkillSerializer


class SkillViewSet(viewsets.ModelViewSet):

    """
    A ViewSet that provides standard actions for  Skills.
    """

    queryset = Skill.objects.all()
    serializer_class = SkillSerializer