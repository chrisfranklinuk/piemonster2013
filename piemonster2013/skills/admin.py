from django.contrib import admin
from .models import Skill

class SkillAdmin(admin.ModelAdmin):
  inlines = []

admin.site.register(Skill, SkillAdmin)
