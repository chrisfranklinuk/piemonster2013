from django.conf.urls import patterns, url
from .views import SkillList, SkillCreate, SkillDetail, SkillUpdate, SkillDelete


urlpatterns = patterns(
    '',
    url(r'^$', SkillList.as_view(), name='skill_list'),
    url(r'^new/$', SkillCreate.as_view(), name='skill_create'),
    url(r'^(?P<pk>\d+)/$', SkillDetail.as_view(), name='skill_detail'),
    url(r'^(?P<pk>\d+)/update/$', SkillUpdate.as_view(), name='skill_update'),
    url(r'^(?P<pk>\d+)/delete/$', SkillDelete.as_view(), name='skill_delete'),
)


from rest_framework import routers
from .api import SkillViewSet

router = routers.SimpleRouter()
router.register(r'api/skill', SkillViewSet)
urlpatterns += router.urls