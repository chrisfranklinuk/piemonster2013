import floppyforms as forms
from .models import Skill


class SkillForm(forms.ModelForm):
    
    """
    Provide form for updating or adding Skill objects
    """

    class Meta:
        model = Skill
