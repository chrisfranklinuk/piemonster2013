import factory
from .models import Skill


class SkillFactory(factory.django.DjangoModelFactory):
    
    """
    Create Skill objects in a uniform, extendable manner.
    """

    FACTORY_FOR = Skill
    FACTORY_DJANGO_GET_OR_CREATE = ('name',)
