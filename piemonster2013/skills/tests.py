from django.core.urlresolvers import reverse
from django_webtest import WebTest
from .factories import SkillFactory
from .models import Skill


class SkillTest(WebTest):
    def test_factory_create(self):
        """
        Test that we can create an instance via the factory.
        """
        instance = SkillFactory.create(name="Test")
        self.assertTrue(isinstance(instance, Skill))

    def test_list_view(self):
        """
        Test that the list view returns at least our factory created instance.
        """
        instance = SkillFactory.create(name="Test")
        response = self.app.get(reverse('skill_list'))
        object_list = response.context['object_list']
        self.assertIn(instance, object_list)

    def test_create_view(self):
        """
        Test that we can create an instance via the create view.
        """
        response = self.app.get(reverse('skill_create'))
        new_name = 'A freshly created thing'

        # check that we don't already have a model with this name
        self.assertFalse(Skill.objects.filter(name=new_name).exists())

        form = response.forms['skill_form']
        form['name'] = new_name
        form.submit().follow()

        instance = Skill.objects.get(name=new_name)
        self.assertEqual(instance.name, new_name)

    def test_detail_view(self):
        """
        Test that we can view an instance via the detail view.
        """
        instance = SkillFactory.create(name="Test")
        response = self.app.get(instance.get_absolute_url())
        self.assertEqual(response.context['object'], instance)

    def test_update_view(self):
        """
        Test that we can update an instance via the update view.
        """
        instance = SkillFactory.create(name='Some old thing')
        response = self.app.get(reverse('skill_update', kwargs={'pk': instance.pk, }))

        form = response.forms['skill_form']
        new_name = 'Some new thing'
        form['name'] = new_name
        form.submit().follow()

        instance = Skill.objects.get(pk=instance.pk)
        self.assertEqual(instance.name, new_name)

    def test_delete_view(self):
        """
        Test that we can delete an instance via the delete view.
        """
        pk = SkillFactory.create(name="Test").pk
        response = self.app.get(reverse('skill_delete', kwargs={'pk': pk, }))
        response = response.form.submit().follow()
        self.assertFalse(Skill.objects.filter(pk=pk).exists())

    def test_api_list_view(self):
        """
        Test that the API list view returns at least our factory created instance.
        """
        instance = SkillFactory.create(name="Test")
        response = self.app.get(reverse('skill-list'))
        object_list = response.context['object_list']
        self.assertIn(instance, object_list)

    def test_api_detail_view(self):
        """
        Test that we can view an instance via the API detail view.
        """
        instance = SkillFactory.create(name="Test")
        response = self.app.get(instance.get_api_url())
        self.assertEqual(response.context['object'], instance)