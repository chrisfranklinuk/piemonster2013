from django.core.urlresolvers import reverse
from vanilla import ListView, CreateView, DetailView, UpdateView, DeleteView
from .forms import SkillForm
from .models import Skill


class SkillCRUDView(object):
    model = Skill
    form_class = SkillForm
    paginate_by = 20

    def get_success_url(self):
        return reverse('skill_list')


class SkillList(SkillCRUDView, ListView):
    pass


class SkillCreate(SkillCRUDView, CreateView):
    pass


class SkillDetail(SkillCRUDView, DetailView):
    pass


class SkillUpdate(SkillCRUDView, UpdateView):
    pass


class SkillDelete(SkillCRUDView, DeleteView):
    pass
