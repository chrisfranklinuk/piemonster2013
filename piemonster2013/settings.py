from os.path import abspath, dirname, join
from configurations import Configuration

PROJECT_ROOT = dirname(dirname(abspath(__file__)))
PROJECT_NAME = 'piemonster2013'


class RedisCache(object):
    CACHES = {
        'default': {
            'BACKEND': 'redis_cache.RedisCache',
            'LOCATION': '127.0.0.1:6379',
            'OPTIONS': {
                'DB': 1,
                'PARSER_CLASS': 'redis.connection.HiredisParser'
            },
        },
    }


class Common(Configuration):
    ADMINS = (
        ('Chris Franklin', 'chris@piemonster.me'),
    )

    MANAGERS = ADMINS

    # SECURITY WARNING: keep the secret key used in production secret!
    SECRET_KEY = 'akjf23r02fnjsdnf'

    # SECURITY WARNING: don't run with debug turned on in production!
    DEBUG = True

    TEMPLATE_DEBUG = True
    ALLOWED_HOSTS = []

    ALLAUTH_APPS = [
        'allauth',
        'allauth.account',
        'allauth.socialaccount',
        # ... include the providers you want to enable:
        # 'allauth.socialaccount.providers.bitly',
        # 'allauth.socialaccount.providers.dropbox',
        # 'allauth.socialaccount.providers.facebook',
        # 'allauth.socialaccount.providers.github',
        # 'allauth.socialaccount.providers.google',
        # 'allauth.socialaccount.providers.instagram',
        # 'allauth.socialaccount.providers.linkedin',
        # 'allauth.socialaccount.providers.openid',
        # 'allauth.socialaccount.providers.persona',
        # 'allauth.socialaccount.providers.soundcloud',
        # 'allauth.socialaccount.providers.stackexchange',
        # 'allauth.socialaccount.providers.twitch',
        # 'allauth.socialaccount.providers.twitter',
        # 'allauth.socialaccount.providers.vimeo',
        # 'allauth.socialaccount.providers.vk',
        # 'allauth.socialaccount.providers.weibo',
    ]

    INSTALLED_APPS = [
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.sites',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'discover_runner',
        'django_jenkins',
        'raven.contrib.django.raven_compat',
        'debug_toolbar',
        'floppyforms',
        'publishing',
        'avatar',
        'south',

        'piemonster2013.projects',
        # 'piemonster2013.userprofiles',
        'piemonster2013.skills',
    ] + ALLAUTH_APPS

    MIDDLEWARE_CLASSES = [
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
        # 'debug_toolbar.middleware.DebugToolbarMiddleware',
    ]

    TEMPLATE_LOADERS = (
            'django.template.loaders.filesystem.Loader',
            'django.template.loaders.app_directories.Loader',
        )

    SITE_ID = 1

    TEMPLATE_CONTEXT_PROCESSORS = [
        'django.contrib.auth.context_processors.auth',
        'django.core.context_processors.request',
        'django.core.context_processors.static',
        'allauth.account.context_processors.account',
        'allauth.socialaccount.context_processors.socialaccount',
    ]

    AUTHENTICATION_BACKENDS = [
        # Needed to login by username in Django admin, regardless of `allauth`
        'django.contrib.auth.backends.ModelBackend',
        # `allauth` specific authentication methods, such as login by e-mail
        'allauth.account.auth_backends.AuthenticationBackend',
    ]

    # Allauth Configuration
    # https://github.com/pennersr/django-allauth
    LOGIN_REDIRECT_URL = "/"
    ACCOUNT_AUTHENTICATION_METHOD = "email"  # "username" | "email" | "username_email"
    ACCOUNT_EMAIL_CONFIRMATION_EXPIRE_DAYS = 7
    ACCOUNT_EMAIL_REQUIRED = True
    ACCOUNT_EMAIL_VERIFICATION = "optional"  # "optional" | "mandatory" | "none"

    ROOT_URLCONF = 'piemonster2013.urls'

    WSGI_APPLICATION = 'piemonster2013.wsgi.application'

    # Database
    # https://docs.djangoproject.com/en//ref/settings/#databases
    # http://django-configurations.readthedocs.org/en/latest/values/#configurations.values.DatabaseURLValue

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': 'dev.sqlite',
        }
    }

    # Internationalization
    # https://docs.djangoproject.com/en//topics/i18n/

    LANGUAGE_CODE = 'en-GB'

    TIME_ZONE = 'Europe/London'

    USE_I18N = True

    USE_L10N = True

    USE_TZ = True

    # Static files (CSS, JavaScript, Images)
    # https://docs.djangoproject.com/en//howto/static-files/

    STATIC_URL = '/static/'
    STATIC_ROOT = join(PROJECT_ROOT, 'static_root')

    MEDIA_URL = '/media/'
    MEDIA_ROOT = join(PROJECT_ROOT, 'media')

    # Additional locations of static files
    STATICFILES_DIRS = [
        join(PROJECT_ROOT, 'static')
    ]

    TEMPLATE_DIRS = [
        join(PROJECT_ROOT, 'templates')
    ]

    TEST_RUNNER = 'discover_runner.DiscoverRunner'

    FIXTURE_DIRS = [
        join(PROJECT_ROOT, 'fixtures')
    ]

    # App settings

    # django-jenkins
    PROJECT_APPS = [app for app in INSTALLED_APPS if app.startswith('piemonster2013.')]
    JENKINS_TASKS = ('django_jenkins.tasks.run_pylint',
                     'django_jenkins.tasks.django_tests',
                     'django_jenkins.tasks.run_pep8',
                     'django_jenkins.tasks.with_coverage')

    # django-debug-toolbar
    DEBUG_TOOLBAR_CONFIG = {'INTERCEPT_REDIRECTS': False}
    INTERNAL_IPS = ('127.0.0.1',)


class Dev(Common):
    DEBUG = True
    TEMPLATE_DEBUG = DEBUG
    EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
    EMAIL_FILE_PATH = '/tmp/app-emails'


class Deployed(RedisCache, Common):
    """
    Settings which are for a non local deployment, served behind nginx.
    """
    PUBLIC_ROOT = join(PROJECT_ROOT, '../public/')
    STATIC_ROOT = join(PUBLIC_ROOT, 'static')
    MEDIA_ROOT = join(PUBLIC_ROOT, 'media')
    COMPRESS_OUTPUT_DIR = ''

    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
    EMAIL_HOST = 'smtp.sendgrid.net'
    EMAIL_PORT = 587
    EMAIL_USE_TLS = True
    EMAIL_HOST_USER = 'chris@piemonster.me'
    EMAIL_HOST_PASSWORD = 'dsak'
    DEFAULT_FROM_EMAIL = ''
    SERVER_EMAIL = ''


class Stage(Deployed):
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': '',
            'USER': '',
            'PASSWORD': '',
            'HOST': 'localhost',
            'OPTIONS': {
                'autocommit': True,  # see https://docs.djangoproject.com/en/dev/ref/databases/#autocommit-mode
            }
        }
    }


class Prod(Deployed):
    DEBUG = False
    TEMPLATE_DEBUG = DEBUG

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': '',
            'USER': '',
            'PASSWORD': '',
            'HOST': 'localhost',
            'OPTIONS': {
                'autocommit': True,  # see https://docs.djangoproject.com/en/dev/ref/databases/#autocommit-mode
            }
        }
    }

    ALLOWED_HOSTS = ['piemonster.me', ]  # add deployment domain here

    RAVEN_CONFIG = {
        'dsn': ''
    }