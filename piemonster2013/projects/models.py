from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _
from autoslug import AutoSlugField
from model_utils.models import TimeStampedModel, TimeFramedModel, StatusModel
from model_utils import Choices

from piemonster2013.skills.models import Skill
from piemonster2013.mixins import ImageFieldMixin


class Project(TimeStampedModel, StatusModel):

    """
    Store and manage Project objects
    """

    STATUS = Choices('active', 'inactive')

    name = models.CharField(
        _('name'), help_text=_('Name of the object'), max_length=255)
    content = models.TextField(
        _('content'),  help_text=_('Longer text content for object'), blank=True, null=True)
    slug = AutoSlugField(populate_from='name')
    skills = models.ManyToManyField(Skill, blank=True)

    class Meta:
        verbose_name = _('Project')
        verbose_name_plural = _('Projects')

    def __unicode__(self):
        return 'Project ({})'.format(self.id or 'Unsaved')

    def get_absolute_url(self):
        return reverse('project_detail', args=[str(self.id)])

    def get_api_url(self):
        return reverse('project-detail', args=[str(self.id)])


class ProjectImage(ImageFieldMixin):

    """
    Store multiple images for a Project.
    """

    project = models.ForeignKey(Project, help_text=_('The project this image is for'), related_name='images')
    default = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        # If default is True then make sure there can be only one default
        if self.default:
            other_images = ProjectImage.objects.filter(project=self.project, default=True)
            for image in other_images:
                image.default = False
                image.save()

        super(ProjectImage, self).save(*args, **kwargs) # Call the "real" save() method.

    def __unicode__(self):
        return 'Project Image ({})'.format(self.id or 'Unsaved')

    def get_api_url(self):
        return reverse('projectimage-detail', args=[str(self.id)])
