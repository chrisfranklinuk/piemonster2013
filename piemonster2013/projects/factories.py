import factory
from .models import Project


class ProjectFactory(factory.django.DjangoModelFactory):
    
    """
    Create Project objects in a uniform, extendable manner.
    """

    FACTORY_FOR = Project
    FACTORY_DJANGO_GET_OR_CREATE = ('name', 'client',)
