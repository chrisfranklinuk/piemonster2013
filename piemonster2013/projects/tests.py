from django.core.urlresolvers import reverse
from django_webtest import WebTest
from .factories import ProjectFactory
from .models import Project

from piemonster2013.clients.factories import ClientFactory


class ProjectTest(WebTest):
    def test_factory_create(self):
        """
        Test that we can create an instance via the factory.
        """
        client = ClientFactory.create(name="Test")
        instance = ProjectFactory.create(name="Test", client=client)
        self.assertTrue(isinstance(instance, Project))

    def test_list_view(self):
        """
        Test that the list view returns at least our factory created instance.
        """
        client = ClientFactory.create(name="Test")
        instance = ProjectFactory.create(name="Test", client=client)
        response = self.app.get(reverse('project_list'))
        object_list = response.context['object_list']
        self.assertIn(instance, object_list)

    def test_create_view(self):
        """
        Test that we can create an instance via the create view.
        """
        client = ClientFactory.create(name="Test")
        response = self.app.get(reverse('project_create'))
        new_name = 'A freshly created thing'

        # check that we don't already have a model with this name
        self.assertFalse(Project.objects.filter(name=new_name).exists())

        form = response.forms['project_form']
        form['name'] = new_name
        form['client'] = client.id
        form.submit()

        instance = Project.objects.get(name=new_name)
        self.assertEqual(instance.name, new_name)

    def test_detail_view(self):
        """
        Test that we can view an instance via the detail view.
        """
        client = ClientFactory.create(name="Test")
        instance = ProjectFactory.create(name="Test", client=client)
        response = self.app.get(instance.get_absolute_url())
        self.assertEqual(response.context['object'], instance)

    def test_update_view(self):
        """
        Test that we can update an instance via the update view.
        """
        client = ClientFactory.create(name="Some old thing")
        instance = ProjectFactory.create(name='Some old thing', client=client)
        response = self.app.get(reverse('project_update', kwargs={'pk': instance.pk, }))

        form = response.forms['project_form']
        new_name = 'Some new thing'
        form['name'] = new_name
        form.submit()

        instance = Project.objects.get(pk=instance.pk)
        self.assertEqual(instance.name, new_name)

    def test_delete_view(self):
        """
        Test that we can delete an instance via the delete view.
        """
        client = ClientFactory.create(name="Test")
        pk = ProjectFactory.create(name="Test", client=client).pk
        response = self.app.get(reverse('project_delete', kwargs={'pk': pk, }))
        response = response.form.submit()
        self.assertFalse(Project.objects.filter(pk=pk).exists())

    def test_api_list_view(self):
        """
        Test that the API list view returns at least our factory created instance.
        """
        client = ClientFactory.create(name="Test")
        instance = ProjectFactory.create(name="Test", client=client)
        response = self.app.get(reverse('project-list'))
        object_list = response.context['object_list']
        self.assertIn(instance, object_list)

    def test_api_detail_view(self):
        """
        Test that we can view an instance via the API detail view.
        """
        client = ClientFactory.create(name="Test")
        instance = ProjectFactory.create(name="Test", client=client)
        response = self.app.get(instance.get_api_url())
        self.assertEqual(response.context['object'], instance)