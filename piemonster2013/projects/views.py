from django.core.urlresolvers import reverse
from vanilla import ListView, CreateView, DetailView, UpdateView, DeleteView
from .forms import ProjectForm, ProjectImageForm
from .models import Project, ProjectImage


class ProjectCRUDView(object):
    model = Project
    form_class = ProjectForm
    paginate_by = 20

    def get_success_url(self):
        return reverse('project_list')


class ProjectList(ProjectCRUDView, ListView):
    pass


class ProjectCreate(ProjectCRUDView, CreateView):
    pass


class ProjectDetail(ProjectCRUDView, DetailView):
    pass


class ProjectUpdate(ProjectCRUDView, UpdateView):
    pass


class ProjectDelete(ProjectCRUDView, DeleteView):
    pass


class ProjectImageCRUDView(object):
    model = ProjectImage
    form_class = ProjectImageForm
    paginate_by = 20

    def get_success_url(self):
        return reverse('projectimage_list')


class ProjectImageCreate(ProjectImageCRUDView, CreateView):
    pass


class ProjectImageUpdate(ProjectImageCRUDView, UpdateView):
    pass


class ProjectImageDelete(ProjectImageCRUDView, DeleteView):
    pass