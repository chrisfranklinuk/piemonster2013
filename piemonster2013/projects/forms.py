import floppyforms as forms
from .models import Project, ProjectImage


class ProjectForm(forms.ModelForm):
    
    """
    Provide form for updating or adding Project objects
    """

    class Meta:
        model = Project


class ProjectImageForm(forms.ModelForm):
    
    """
    Provide form for updating or adding ProjectImage objects
    """

    class Meta:
        model = ProjectImage
