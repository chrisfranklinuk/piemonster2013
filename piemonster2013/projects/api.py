from rest_framework import viewsets

from .models import Project, ProjectImage
from .serializers import ProjectSerializer, ProjectImageSerializer


class ProjectViewSet(viewsets.ModelViewSet):

    """
    A ViewSet that provides standard actions for  Projects.
    """

    queryset = Project.objects.all()
    serializer_class = ProjectSerializer


class ProjectImageViewSet(viewsets.ModelViewSet):

    """
    A ViewSet that provides standard actions for  ProjectImage.
    """

    queryset = ProjectImage.objects.all()
    serializer_class = ProjectImageSerializer