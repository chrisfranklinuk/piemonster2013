from django.conf.urls import patterns, url
from .views import ProjectImageCreate, ProjectImageUpdate, ProjectImageDelete
from .views import ProjectList, ProjectCreate, ProjectDetail, ProjectUpdate, ProjectDelete


urlpatterns = patterns(
    '',
    url(r'^image/new/$', ProjectImageCreate.as_view(), name='projectimage_create'),
    url(r'^image/(?P<pk>\d+)/update/$', ProjectImageUpdate.as_view(), name='projectimage_update'),
    url(r'^image/(?P<pk>\d+)/delete/$', ProjectImageDelete.as_view(), name='projectimage_delete'),

    url(r'^$', ProjectList.as_view(), name='project_list'),
    url(r'^new/$', ProjectCreate.as_view(), name='project_create'),
    url(r'^(?P<pk>\d+)/$', ProjectDetail.as_view(), name='project_detail'),
    url(r'^(?P<pk>\d+)/update/$', ProjectUpdate.as_view(), name='project_update'),
    url(r'^(?P<pk>\d+)/delete/$', ProjectDelete.as_view(), name='project_delete'),
)


from rest_framework import routers
from .api import ProjectViewSet, ProjectImageViewSet

router = routers.SimpleRouter()
router.register(r'api/project', ProjectViewSet)
router.register(r'api/project/image', ProjectImageViewSet)
print router.urls
urlpatterns += router.urls