from rest_framework import serializers
from .models import Project, ProjectImage


class ProjectImageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ProjectImage
        #fields = ('name',)


class ProjectSerializer(serializers.HyperlinkedModelSerializer):
    images = ProjectImageSerializer

    class Meta:
        model = Project
        #fields = ('name',)
