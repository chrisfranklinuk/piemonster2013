from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.views.generic import TemplateView
from django.contrib import admin


admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', TemplateView.as_view(template_name='home.html')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^projects/', include('piemonster2013.projects.urls')),
    url(r'^skills/', include('piemonster2013.skills.urls')),
    #url(r'^people/', include('piemonster2013.userprofiles.urls')),
    url(r'^avatar/', include('avatar.urls')),
    url(r'^', include('publishing.urls')),
)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)