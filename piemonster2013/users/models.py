# -*- coding: utf-8 -*-
# Import the AbstractUser model
from django.contrib.auth.models import AbstractUser

# Import the basic Django ORM models library
from django.db import models

from django.utils.translation import ugettext_lazy as _


# Subclass AbstractUser
class User(AbstractUser):
	content = models.TextField(
        _('content'),  help_text=_('Longer text content for object'), blank=True, null=True)
	public = models.BooleanField(default=False)
	# TODO: Add user type field - use status field to get free manager

    def __unicode__(self):
        return self.username