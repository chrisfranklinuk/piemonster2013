from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone


class PersonMixin(models.Model):
    first_name = models.CharField(
        _(u'first name'),
        max_length=256, blank=True)
    last_name = models.CharField(
        _(u'last name'),
        max_length=256, blank=True)
    job_title = models.CharField(
        _(u'job title'),
        max_length=256, blank=True)
    notes = models.TextField(
        _(u'notes'),
        blank=True)

    class Meta:
        abstract = True

    def get_full_name(self):
        return ("%s %s" %(self.first_name, self.last_name))


class ImageFieldMixin(models.Model):
    image = models.ImageField(
        _(u'image'),
        upload_to='uploaded_images'
        )
    content = models.TextField(
        _(u'content'),
        blank=True)

    class Meta:
        abstract = True

